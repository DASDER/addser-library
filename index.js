



  



      
      









module.exports = {
    ConversionDate: function (datein, dateinformat, dateinsep, dateoutformat, dateoutsep) {
           
        try{
      
      if(dateinsep != "aucunsep" && dateinsep != "/"){
        throw new Error("ErrCodeCnvDate 2 : dateinsep invalid, seul les separateurs '/' et 'aucunsep' sont permis")
      }
      
      if(dateoutsep != "aucunsep" && dateoutsep != "/"){
        throw new Error("ErrCodeCnvDate 2 : dateoutsep invalid, seul les separateurs '/' et 'aucunsep' sont permis")
      }
      
      if(dateinformat != "JavaScript" && dateinformat != "YMD" && dateinformat != "DMY"){
        throw new Error("ErrCodeCnvDate 3 : dateinformat invalid, seul les format 'JavaScript' , 'YMD' et 'DMY' sont permis")
      }
      
      if( dateoutformat != "YMD" && dateinformat != "DMY"){
        throw new Error("ErrCodeCnvDate 3 : dateinformat invalid, seul les format 'YMD' et 'DMY' sont permis")
      }
      
      
      
      
       let day
       let month
       let year
      
       if (datein == '') {
         return '';
       }
       if (datein == undefined) {
         return '';
       }
       if (datein == 0) {
         return '';
       }
      
       if (datein == "0") {
        return '';
      }
      
      //Added to handle date as numbers
      datein = datein.toString()
      
       switch (dateinformat) {
         //format date entrée: format JavaScript
         case 'JavaScript':
           day = datein.getDate().toString();
           month = (datein.getMonth() + 1).toString();
      
           if (day.length == 1) {
             day = '0' + day;
           }
           if (month.length == 1) {
             month = '0' + month;
           }
      
           year = datein.getFullYear().toString();
           break;
         case 'YMD':
           if (dateinsep == '/') {
             //format date entrée: AAAA/MM/JJ
             year = datein.substring(0, 4);
             month = datein.substring(5, 7);
             day = datein.substring(8, 10);
           }
           if (dateinsep == 'aucunsep') {
             //format date entrée: AAAAMMJJ
             year = datein.substring(0, 4);
             month = datein.substring(4, 6);
             day = datein.substring(6, 8);
           }
      
           break;
         case 'DMY':
           if (dateinsep == '/') {
             //format date entrée: JJ/MM/AAAA
             day = datein.substring(0, 2);
             month = datein.substring(3, 5);
             year = datein.substring(6, 10);
           }
           if (dateinsep == 'aucunsep') {
             //format date entrée: JJMMAAAA
             day = datein.substring(0, 2);
             month = datein.substring(2, 4);
             year = datein.substring(4, 8);
           }
      
           break;
       }
      
       if(isValidDate(Number(year), Number(month), Number(day))){
        throw new Error("ErrCodeCnvDate 1 : Date invalid")
       }
      
       switch (dateoutformat) {
         case 'YMD':
           if (dateoutsep == '/') {
             return year.concat(dateoutsep, month, dateoutsep, day);
           }
      
           if (dateoutsep == 'aucunsep') {
             return year.concat(month, day);
           }
      
         case 'DMY':
           if (dateoutsep == '/') {
             return day.concat(dateoutsep, month, dateoutsep, year);
           }
      
           if (dateoutsep == 'aucunsep') {
             return day.concat(month, year);
           }
           
          default:
           return '';
            
       }
          
        }
        
        catch(e){ 
     console.log(e)    
      return ''}


      function isValidDate(year, month, day) {
        let d = new Date(year, month-1, day);
        if (d.getFullYear() == year && d.getMonth() == month-1 && d.getDate() == day) {
            return true;
        }
        return false;
      }

      },
    SortingArraysByMultipleColumns:   function (Tab, ...arrayOfColumns){
  
    let SortStartTimer = performance.now();    
    let WorkingTab = JSON.parse(JSON.stringify(Tab))
    
    WorkingTab.sort((a,b)=> {
      let sortOrder = a[arrayOfColumns[0]].localeCompare(b[arrayOfColumns[0]]);
      for(let i=1 ; i<arrayOfColumns.length; i++){
        sortOrder = sortOrder || a[arrayOfColumns[i]].localeCompare(b[arrayOfColumns[i]])
      }
    
      return sortOrder
    
    })
    
    let SortFinishTimer = performance.now() -  SortStartTimer;
    let WorkingTabRowsLength =  WorkingTab.length
    let WorkingTabColumnsLength =WorkingTab.length
    let SortKPI = (WorkingTabRowsLength * WorkingTabColumnsLength)/(SortFinishTimer*SortFinishTimer)
    
    // console.log("Original Array contains ", WorkingTabRowsLength , "row(s)")
    // console.log("Orginal Array contains ", WorkingTabColumnsLength, "columns")
    // console.log("Executed in: ", SortFinishTimer, " Milisecond(s)")
    // console.log("Sort KPI ((rows*columns)/executiontime²): ", SortKPI  )
    // console.log("Sorted by:  ", arrayOfColumns) 
    
    return WorkingTab;
      },

     AggregationTab:   function (Tab, TabKeys, TabAggregation){


    let Tabaggregatfilter = [] ;

    function Sum(array){
      let somme = 0;
      array.forEach(element => {somme += element[key]})
      return somme
    }

   Tab.forEach((element, index, array) => {

    let entries = []

    TabKeys.forEach(key => entries.push([key, element[key]]))
    TabAggregation.forEach(Aggregation => entries.push([Aggregation,Sum(array.filter(element2 => {
      let keep = true;
      TabKeys.forEach(key => keep = keep && (element2[key] == element[key]) )
      return keep

    } ), Aggregation)]) )

    for (const property in Tab[0]) {
      if(!(TabKeys.includes(property) || TabAggregation.includes(property))){
        entries.push([property, element[property]])
      }
          }

    Tabaggregatfilter.push(Object.fromEntries(entries));
    
    })
    
    Tabaggregatfilter =Tabaggregatfilter.filter((value, index, self) =>
      index === self.findIndex((t) => 
      {
        let keep = true;
        TabKeys.concat(TabAggregation).forEach(key => keep = keep && t[key] == value[key] )
        return keep
      }
      
      )
    )

return Tabaggregatfilter;


      },

      ConversionNum: function(numberin, numberdecimale, formatespace, formatdec){
        if(numberin == undefined || numberin == '' || isNaN(Number(numberin))){
          return "0"
        }
        let WorkingNumber = Number(numberin)
        let temp = separate(WorkingNumber.toFixed(numberdecimale), formatespace )
        return temp.replace(".", formatdec)
        
        function separate(val, sep) {
          // remove sign if negative
          var sign = 1;
          if (val < 0) {
            sign = -1;
            val = -val;
          }
          // trim the number decimal point if it exists
          let num = val.toString().includes('.') ? val.toString().split('.')[0] : val.toString();
          let len = num.toString().length;
          let result = '';
          let count = 1;
        
          for (let i = len - 1; i >= 0; i--) {
            result = num.toString()[i] + result;
            if (count % 3 === 0 && count !== 0 && i !== 0) {
              result = sep + result;
            }
            count++;
          }
        
          // add number after decimal point
          if (val.toString().includes('.')) {
            result = result + '.' + val.toString().split('.')[1];
          }
          // return result with - sign if negative
          return sign < 0 ? '-' + result : result;
        }
        
        
        
        
        
        },
};

