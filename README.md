<h1>Bienvenue a la librarie Addser</h1>
<br>
La librarie Addser contient des differentes fonction pré-codé pour utiliser.
<br>

<h2>Comment utiliser</h2>

1. Ajouter le module addserlibrary via la commande:

_npm i addserlibrary_

2. Importer le module:

import * as AddserLibrary from 'addserlibrary'

3. Utilisation des fonctions:

<br>

   `let SortedArray = AddserLibrary.SortingArraysByMultipleColumns(Array, "name", "alphabet")
   `

 


<h3>Fonctions</h3>

Version 3 :
<br>

Nom de fonction => **AggregationTab**
<br>
Description => **Aggrege un Tableau par clés en cumulants des Zones**
<br>
Entrés de fonction => **Tableau à aggreger** , **Tableau de colonne clés** , **Tableau de colonnes à cumuler**
<br>
Sortie de fonction => **Tableau Aggregé**
<br>
Exemple:
<br>

    ` let AgreagatedArray = AddserLibrary.AggregationTab(Array, ["name"], ["count"])
    console.log(AgreagatedArray)`


<br>
<br>
<br>

Nom de fonction => **ConversionDate**
<br>
Description => **Convertie une date en format x vers une format y**
<br>
Entrés de fonction => **Date à convertir** , F**ormat de date à convertir** , **Separateur de date à convertir** , **Format de date sortie**, **Separateur de date de sortie**
<br>
Sortie de fonction => **Date de sortie**
<br>
Exemple:

<br>

    `let Dateformated = AddserLibrary.ConversionDate("20220202", "YMD", "aucunsep", "DMY", "/")
    console.log(Dateformated)`

<br>
<br>
<br>  

Nom de fonction => **SortingArraysByMultipleColumns**
<br>
Description => **Trie un tableau par plusieurs colonnes**
<br>
Entrés de fonction => **Tableau à trier**, **Colonnes a triés**...
<br>
Sortie de fonction => **Tableau trié**
<br>
Exemple:

<br>

    `let SortedArray = AddserLibrary.SortingArraysByMultipleColumns(Array, "name", "alphabet")
    console.log(SortedArray)`

<h3>Lien Utilies</h3>

  [Package NPM](https://www.npmjs.com/package/addserlibrary?activeTab=readme)
  <br>
  [Gitlab](https://gitlab.com/DASDER/addser-library)
  <br>
  Website (En construction) 

    
